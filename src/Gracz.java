/**
 * Created by damianszwichtenberg on 27.04.2016.
 */
public class Gracz {
    private String imie;
    private int wynikGracza;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWynikGracza() {
        return wynikGracza;
    }

    public void setWynikGracza(int wynikGracza) {
        this.wynikGracza = wynikGracza;
    }
}
