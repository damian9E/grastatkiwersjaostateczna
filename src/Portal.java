import java.util.ArrayList;

/**
 * Created by damianszwichtenberg on 27.04.2016.
 */
public class Portal {
    private String nazwa;
    private ArrayList<String> pozycje;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public ArrayList<String> getPozycje() {
        return pozycje;
    }

    public void setPozycje(ArrayList<String> pozycje) {
        this.pozycje = pozycje;
    }
}
