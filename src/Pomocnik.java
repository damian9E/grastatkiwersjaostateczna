import java.util.Scanner;

/**
 * Created by damianszwichtenberg on 27.04.2016.
 */
public class Pomocnik {
    private String odczyt;

    public String getOdczyt() {
        Scanner input = new Scanner(System.in);
        odczyt = input.nextLine();
        while ( odczyt.length() != 2 )  {
            System.out.println("Podaj nowy typ: ");
            odczyt = input.nextLine();
        }
        return odczyt;
    }
}
